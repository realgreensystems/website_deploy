﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DeployWebsites.DataAccess
{
    public static class DatabaseService
    {
        private static SqlConnection CreateConnection(string connectionString)
        {
            var db = new SqlConnection(connectionString);
            db.Open();
            return db;
        }

        public static SqlCommand CreateCommand(string connectionString)
        {
            var cmd = CreateConnection(connectionString).CreateCommand();

            cmd.Disposed += cmd_Disposed;
            return cmd;
        }


        private static void cmd_Disposed(object sender, EventArgs e)
        {
            var cmd = (SqlCommand)sender;
            cmd.Connection.Close();
            cmd.Connection.Dispose();

            cmd.Disposed -= cmd_Disposed;
        }
    }
}
