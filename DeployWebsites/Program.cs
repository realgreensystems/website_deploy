﻿using DeployWebsites.DataAccess;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace DeployWebsites
{
    class Program
    {
        static List<KeyValuePair<int, string>> codeBasePaths = new List<KeyValuePair<int, string>>();

        static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SA"].ToString();
            var clusterName = ConfigurationManager.AppSettings["Cluster"].ToString();
            var iisSiteName = ConfigurationManager.AppSettings["IISWebsiteName"];
            var qaSites = ConfigurationManager.AppSettings["QASites"] ?? "0";
            if(string.IsNullOrWhiteSpace(iisSiteName))
            {
                iisSiteName = "Default Web Site";
            }
            //int iCounter = 21;
            //var totalValue = "";
            //while (iCounter < 1001)
            //{
            //    iCounter = iCounter + 1;
            //    var stringValue = "/C c:\\windows\\system32\\inetsrv\\AppCmd\\ add app /site.name:\"Default Web Site\" /path:/" + iCounter.ToString() + " /physicalPath:\"c:\\ctemp\\" + iCounter.ToString() + "\"";
            //    totalValue += Environment.NewLine + stringValue;
            //    //ExecuteCommand("/C mkdir c:\\ctemp\\" + iCounter.ToString());
            //    ExecuteCommand(stringValue);
            //}
            List<SiteDetails> siteDetails = new List<SiteDetails>();
            ServerManager serverManager = new ServerManager();

            // get the site (e.g. default)
            Site sites = serverManager.Sites.FirstOrDefault(s => s.Name.Trim().ToLower() == iisSiteName.Trim().ToLower());
            List<string> physicalPaths = new List<string>();
            foreach (var site in sites.Applications)
            {
                var sitePath = site.VirtualDirectories[0].PhysicalPath;
                if (!sitePath.Trim().ToUpper().StartsWith("%SYSTEMDRIVE%") && site.Path.Replace("/", "").Trim().ToUpper() != ("SERVERID") && !string.IsNullOrWhiteSpace(site.Path.Replace("/","")))
                {
                    physicalPaths.Add(sitePath);
                }

            }
            StartOrStopIIS(false);
            var siteCounter = 0;
            int numberOfSitesPerCodeBase = 2;
            Int32.TryParse(ConfigurationManager.AppSettings["SitesPerCodebase"], out numberOfSitesPerCodeBase);
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry(" Remove existing Sym links", EventLogEntryType.Information, 101, 1);
            }
            foreach (var sitePath in physicalPaths)
            {
                try
                {
                    if (!sitePath.Trim().ToUpper().StartsWith("%SYSTEMDRIVE%") && sitePath.Trim().ToUpper() != "D:\\SYMBOLICHOSTEDSA5INSTALLS")
                    {
                        siteCounter = siteCounter + 1;
                        var sourceDir = GetCodeBaseDirectory(siteCounter, numberOfSitesPerCodeBase);
                        System.IO.DirectoryInfo sourceDirectory = new DirectoryInfo(sourceDir);
                        siteDetails.Add(new SiteDetails() { CodeBase = sourceDir, SiteName = sitePath, SitePath = sitePath });

                        System.IO.DirectoryInfo di = new DirectoryInfo(sitePath);
                        foreach (DirectoryInfo dir in di.GetDirectories())
                        {
                            if (dir.Name.Trim().ToUpper() != "APP_DATA")
                            {
                                dir.Delete(true);
                            }
                        }
                        foreach (FileInfo file in di.EnumerateFiles())
                        {
                            if (!file.Name.Trim().ToUpper().EndsWith("WEB.TRANSFORM.CONFIG"))
                            {
                                file.Delete();
                            }
                        }
                    }
                }
                catch (Exception ex1)
                {
                    LogError(connectionString, sitePath, "Cluster : " + clusterName + ". " + ex1.Message);
                    using (EventLog eventLog = new EventLog("Application"))
                    {
                        eventLog.Source = "Application";
                        eventLog.WriteEntry(DateTime.Now.ToString() + " ERROR Deleting existing sym links : " + ex1.Message, EventLogEntryType.Information, 101, 1);
                    }
                }
            }
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry(" Creating Sym links", EventLogEntryType.Information, 101, 1);
            }
            var transformerSource = ConfigurationManager.AppSettings["TransformerSource"];

            Parallel.ForEach(physicalPaths, sitePath =>
            {
                if (!sitePath.Trim().ToUpper().StartsWith("%SYSTEMDRIVE%") && sitePath.Trim().ToUpper() != "D:\\SYMBOLICHOSTEDSA5INSTALLS")
                {
                    var individualSite = siteDetails.Where(x => x.SitePath == sitePath).FirstOrDefault();
                    try
                    {
                        System.IO.DirectoryInfo sourceDirectory = new DirectoryInfo(individualSite.CodeBase);
                        foreach (DirectoryInfo dir in sourceDirectory.GetDirectories())
                        {
                            if (dir.Name.Trim().ToUpper() != "APP_DATA")
                            {
                                ExecuteCommand("/C mklink /d \"" + sitePath + "\\" + dir.Name + "\" \"" + dir.FullName + "\"");
                            }
                        }
                        foreach (FileInfo file in sourceDirectory.EnumerateFiles())
                        {
                            if (!file.Name.Trim().ToUpper().EndsWith("WEB.CONFIG") && !file.Name.Trim().ToUpper().EndsWith("WEB.TRANSFORM.CONFIG"))
                            {
                                ExecuteCommand("/C mklink /h \"" + sitePath + "\\" + file.Name + "\" \"" + file.FullName + "\"");
                            }
                        }
                        ExecuteCommand("/C " + transformerSource + "//transformer.exe " + sourceDirectory + "\\web.config " + individualSite.SitePath + "\\web.transform.config " + individualSite.SitePath + "\\web.config");
                        if (qaSites == "1")
                        {
                            UpdateConfigRemoveRewrite(sitePath + "\\web.config");
                        }
                    }
                    catch (Exception ex1)
                    {
                        LogError(connectionString, sitePath, "Cluster : " + clusterName + ". " + ex1.Message);
                        using (EventLog eventLog = new EventLog("Application"))
                        {
                            eventLog.Source = "Application";
                            eventLog.WriteEntry(DateTime.Now.ToString() + " ERROR Creating sym links and transformation for " + sitePath + ": " + " : " + individualSite.CodeBase + ex1.StackTrace, EventLogEntryType.Information, 101, 1);
                        }
                    }
                }
            });

            StartOrStopIIS(true);
        }
        public static void AddOrUpdateCodeBase()
        {
            var mainCodeBaseDirectory = ConfigurationManager.AppSettings["Codebases"];
            var masterSource = ConfigurationManager.AppSettings["MasterSourceFiles"];

            var existingCodeBasePaths = codeBasePaths.Count();
            var newCodeBase = mainCodeBaseDirectory + "\\" + (existingCodeBasePaths + 1).ToString().Trim() + "-SA5";

            codeBasePaths.Add(new KeyValuePair<int, string>((existingCodeBasePaths + 1), newCodeBase));
            
            CopyCodebases(masterSource, newCodeBase);
        }
        public static string GetCodeBaseDirectory(decimal iCounter, decimal numberOfSitesPerCodeBase)
        {
            var existingCodeBasePaths = codeBasePaths.Count();
            decimal totalCodeBases = iCounter / numberOfSitesPerCodeBase;
            if (existingCodeBasePaths < totalCodeBases)
            {
                AddOrUpdateCodeBase();
                existingCodeBasePaths = codeBasePaths.Count();
            }
            return codeBasePaths.Where(x => x.Key == Math.Ceiling(totalCodeBases)).FirstOrDefault().Value;
        }
        public static void ExecuteCommand(string cmdCommand)
        {
            Process p = new Process();
            p.StartInfo.Arguments = cmdCommand;
            p.StartInfo.FileName = "CMD.EXE";
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.Verb = "runas";
            p.Start();
            p.WaitForExit();
        }
        public static void CopyCodebases(string source, string destination)
        {
            Console.WriteLine("Source :" + source);
            Console.WriteLine(string.Format("/C Robocopy \"{0}\" \"{1}\" /mir /R:2 /W:2", source, destination));
            ExecuteCommand(string.Format("/C Robocopy \"{0}\" \"{1}\" /mir /R:2 /W:2", source, destination));
        }

        public static void StartOrStopIIS(bool startIIS)
        {
            ExecuteCommand(string.Format("/C IISRESET /{0}", startIIS ? "Start" : "Stop"));
        }
        public static void LogError(string connectionString, string customerNumber, string errorMessage)
        {
            using (var cmd = DatabaseService.CreateCommand(connectionString))
            {
                string sqlQuery = "Insert into [ErrorsDuringDeploy] (cust_no, errormessage) select @custno, @errormsg";
                cmd.CommandText = sqlQuery;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@custno", customerNumber);
                cmd.Parameters.AddWithValue("@errormsg", errorMessage);
                cmd.ExecuteNonQuery();

                if (cmd.Connection != null)
                    cmd.Connection.Close();
                if (cmd.Connection != null)
                    cmd.Connection.Dispose();
                if (cmd != null)
                    cmd.Dispose();
            }
        }
        public class SiteDetails
        {
            public string SiteName { get; set; }
            public string SitePath { get; set; }
            public string CodeBase { get; set; }
        }
        public static void UpdateConfigRemoveRewrite(string path)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(path);
                var nodes = doc.GetElementsByTagName("rewrite");
                for (int i = 0; i < nodes.Count; i++)
                {
                    nodes[i].ParentNode.RemoveChild(nodes[i]);
                }
                doc.Save(path);
            }
            catch (Exception ex)
            {

            }
        }
    }
}